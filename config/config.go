package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment         string // develop, staging, production
	PostgresHost        string
	PostgresPort        int
	PostgresUser        string
	PostgresDB          string
	PostgresPassword    string

	CustomerServicePort string
	CustomerServiceHost string

	SalonServicePort string
	SalonServiceHost string

	AutoInfoServicePort string
	AutoInfoServiceHost string

	CtxTimeout          int // context timeout in seconds
	LogLevel            string
	HTTPPort            string
	RedisHost           string
	RedisPort           string
	SignInKey           string
	AuthConfigPath      string
	EmailPassword       string
	CSVFilePath         string
	AWSRegion           string
	AWSAccessKeyId      string
	AWSSecretAccessKey  string
}

func Load() Config{
	
	c := Config{}
	
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(GetOrReturnDefault("HTTP_PORT", ":9191"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "citizenfour"))
	c.PostgresDB = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "customer_evron"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "12321"))

	c.SignInKey = cast.ToString(GetOrReturnDefault("SIGNINKEY", "MirobidSignInKey"))
	c.CtxTimeout = cast.ToInt(GetOrReturnDefault("CTX_TIMEOUT", 7))
	c.AuthConfigPath = cast.ToString(GetOrReturnDefault("AUTH_PATH", "./config/auth.conf"))
	c.CSVFilePath = cast.ToString(GetOrReturnDefault("CSV_FILE_PATH", "./config/auth.csv"))

	c.CustomerServiceHost = cast.ToString(GetOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToString(GetOrReturnDefault("CUSTOMER_SERVICE_PORT", "3333"))

	c.SalonServiceHost = cast.ToString(GetOrReturnDefault("SALON_SERVICE_HOST", "localhost"))
	c.SalonServicePort = cast.ToString(GetOrReturnDefault("SALON_SERVICE_PORT", "4444"))

	c.AutoInfoServiceHost = cast.ToString(GetOrReturnDefault("AUTO_INFO_SERVICE_HOST", "localhost"))
	c.AutoInfoServicePort = cast.ToString(GetOrReturnDefault("AUTO_INFO_SERVICE_PORT", "1111"))

	c.RedisHost = cast.ToString(GetOrReturnDefault("REDIS_HOST", "redis"))
	c.RedisPort = cast.ToString(GetOrReturnDefault("REDIS_PORT", "6379"))


return c

}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}