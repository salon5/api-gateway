update_submodule:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:salon5/protos.git

proto-gen:
	./script/gen-proto.sh

swag:
	swag init -g api/router.go -o api/docs