package main

import (
	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/salon5/api-gateway/api"
	"gitlab.com/salon5/api-gateway/config"
	"gitlab.com/salon5/api-gateway/pkg/logger"
	"gitlab.com/salon5/api-gateway/services"
	r "gitlab.com/salon5/api-gateway/storage/redis"
)

func main() {

	var (
		casbinEnforcer *casbin.Enforcer
	)
	
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api-gateway")
	
	casbinEnforcer, err := casbin.NewEnforcer(cfg.AuthConfigPath, cfg.CSVFilePath)
	if err != nil {
		log.Error(`casbin enforcer error`, logger.Error(err))
		return
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error(`Error casbin load Policy`, logger.Error(err))
		return 
	}

	serviceManager, err := services.NewServiceManager(&cfg)
	if err != nil {
		log.Error(`gRpc dial Error`, logger.Error(err))
	}

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)

	pool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", cfg.RedisHost + ":" + cfg.RedisPort)
		},
	}

	server := api.New(api.Option{
		Conf: cfg,
		Logger: log,
		ServiceManager: serviceManager,
		Redis: r.NewRedisRepo(pool),
		CasbinEnforcer: casbinEnforcer,
	})

	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal(`failed to run http server`, logger.Error(err))
		panic(err)
	}


}