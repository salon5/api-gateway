package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/salon5/api-gateway/api/models"
	"gitlab.com/salon5/api-gateway/genproto/customer"
	"gitlab.com/salon5/api-gateway/genproto/salon"
	"gitlab.com/salon5/api-gateway/pkg/logger"
)


func (h *handlerV1) CreateSalon(c *gin.Context) {
	_, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Code:    http.StatusInternalServerError,
			Message: "Ivalid access token",
			Error:   err,
		})
		h.log.Error("Error while getting claims of access token", logger.Error(err))
		return
	}

	body := &salon.CreateSalonResp{}
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.FailureInfo{
			Code:    http.StatusBadRequest,
			Message: "Enter right info",
			Error:   err,
		})
		h.log.Error("Error while binding json")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce, err := h.serviceManager.SalonService().CreateSalon(ctx, &salon.CreateSalonReq{
		Id: body.Id,
		CustomerId: body.CustomerId,
		AutoId: body.AutoId,
		SalonName: body.SalonName,
		Description: body.Description,
	})
	for _, res := range responce.Locations{
		temp := &customer.Location{
			LocationId: res.LocationId,
			SalonId: res.SalonId,
			Country: res.Country,
			Street: res.Street,
			District: res.District,
			PhoneNumber: res.District,
		}
		responce.Locations = append(responce.Locations, (*salon.Location)(temp))
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.FailureInfo{
			Code:    http.StatusInternalServerError,
			Message: "Something went wrong",
			Error:   err,
		})
		h.log.Error("Error while Creating COMMENT", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, responce)
}

