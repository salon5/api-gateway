package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	v1 "gitlab.com/salon5/api-gateway/api/handlers/v1"
	"gitlab.com/salon5/api-gateway/api/middleware"
	token "gitlab.com/salon5/api-gateway/api/tokens"
	"gitlab.com/salon5/api-gateway/config"
	"gitlab.com/salon5/api-gateway/pkg/logger"
	"gitlab.com/salon5/api-gateway/services"
	"gitlab.com/salon5/api-gateway/storage/repo"
	"github.com/swaggo/gin-swagger"
	swaggerFiles "github.com/swaggo/files"

)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
	CasbinEnforcer *casbin.Enforcer
}

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := token.JWTHandler{
		SigninKey: option.Conf.SignInKey,
		Log:       option.Logger,
	}

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		JWTHandler:     jwtHandler,
	})

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwtHandler, config.Load()))

	router.MaxMultipartMemory = 8 << 20 // = 8 Mib
	api := router.Group("/v1")
	
	//Salon
	api.POST("/salon/created", handlerV1.CreateSalon)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	return router

}
