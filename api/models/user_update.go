package models

type UpdateUserProfileReq struct {
	FullName     string `json:"full_name"`
	UserName     string `json:"username"`
	ProfilePhoto string `json:"prile_photo"`
}

type UpdatUserPhoneNumberReq struct {
	PhoneNumber string `json:"phone_number" example:"998990001122"` 
}

type SavePhoneNumberInRedis struct {
	UserId     string `json:"user_id"`
	PhoneNumber string `json:"phone_number"`
	Code        string `json:"code"`
}

type UpdateUserPasswordReq struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}
