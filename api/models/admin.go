package models

type Admins struct {
	AdminName string `json:"admin_name" example:"admin"`
	Password  string `json:"password" example:"ADFakj23a9823@#41asf"`
	Role      string `json:"role" example:"admin/superadmin"`
}

type AdminRes struct {
	AdminName   string `json:"admin_name" example:"admin"`
	Role        string `json:"role" example:"admin/superadmin"`
	AccessToken string `json:"access_token"`
}

type AllAdmins struct {
	Admins []*Admins `json:"admins"`
}
