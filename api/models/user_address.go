package models

type UserAddressReq struct {
	Country string `json:"country"`
	Region  string `json:"region"`
}

type UpdateUserAddressReq struct {
	Id      string `json:"id"`
	Country string `json:"country"`
	Region  string `json:"region"`
}
