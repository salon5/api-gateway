package models

type CreateCommentInfo struct {
	PostId  int64  `json:"post_id"`
	Comment string `json:"comment"`
}

type UpdateCommentReq struct {
	CommentId string `json:"id"`
	Comment   string `json:"comment"`
	CommentatorId   string   `json:"commentator_id"`
	CommentatorType string   `json:"commentator_type"`
}

type GetCommentInfo struct {
	CommentId       string   `json:"id"`
	CommentatorId   string   `json:"commentator_id"`
	CommentatorType string   `json:"commentator_type"`
	PostId          int64    `json:"post_id"`
	Comment         string   `json:"comment"`
	ProfilePhoto    string   `json:"profile_photo"`
	FullName        string   `json:"full_name"`
	AnswerInfo      []Answer `json:"answer_info"`
	CreatedAT       string   `json:"created_at"`
	UpdateAT        string   `json:"updated_at"`
}

type Answer struct {
	AnswerId        string `json:"answer_id"`
	CommentId       string `json:"comment_id"`
	CommentatorId   string `json:"commentator_id"`
	CommentatorType string `json:"commentator_type"`
	AnswerComment   string `json:"answer_comment"`
	ProfilePhoto    string `json:"profile_photo"`
	FullName        string `json:"full_name"`
	CreatedAT       string `json:"created_at"`
	UpdateAT        string `json:"updated_at"`
}

type DeleteComment struct {
	CommentId string `json:"comment_id"`
}

type CreateReplyCommentInfo struct {
	CommentId     string `json:"comment_id"`
	AnswerComment string `json:"answer_comment"`
}

type UpdateCommentReplyReq struct {
	AnswerId      string `json:"answer_id"`
	AnswerComment string `json:"answer_comment"`
	CommentatorId string `json:"commentator_id"`
}

type DeleteReplyComment struct {
	AnswerId string `json:"answer_id"`
}
