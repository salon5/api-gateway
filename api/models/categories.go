package models

type CategoryReq struct {
	Category string `json:"category_name" example:"Electronics"`
	PhotoURL string `json:"photo_url" example:"https://www.example.com/myimage.jpg"`
}

type UpdateCategoryReq struct {
	Id       int    `json:"id" example:"1"`
	Category string `json:"category_name" example:"Electronics"`
	PhotoURL string `json:"photo_url" example:"https://www.example.com/myimage.jpg"`
}

type TypeReq struct {
	CategoryId int    `json:"category_id" example:"1"`
	TypeName   string `json:"type_name" example:"Computer"`
}

type TypesRes struct {
	Id         int    `json:"id"`
	CategoryId int    `json:"category_id"`
	TypeName   string `json:"type_name"`
}

type UpdateTypeReq struct {
	Id         int    `json:"id" example:"1"`
	CategoryId int    `json:"category_id" example:"1"`
	TypeName   string `json:"type_name" example:"Computer"`
}
