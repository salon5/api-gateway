package models

type UserRegisterReq struct {
	PhoneNumber string `json:"salon_name"`
}

type SaveForgottenUserPasInRedis struct {
	Code        string `json:"code"`
	PhoneNumber string `json:"phone_number" binding:"required"`
}

type SetForgottenUserPasReq struct {
	Code        string `json:"code"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	NewPassword string `json:"new_password"`
}
