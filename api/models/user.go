package models

type SaveUserInRedis struct {
	Id          string `json:"id"`
	FullName    string `json:"full_name"`
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
	Code        string `json:"code"`
}

type RegisterUserLastStepReq struct {
	Password string `json:"password"`
	FullName string `json:"full_name"`
}


type UpdateUserPhotoReq struct {
	PhotoUrl string `json:"photo_url"`
}

type UpdateUserUsernameReq struct {
	Username string `json:"username"`
}

type UpdatUserEmailReq struct {
	Email string `json:"email"`
}

type SaveEmailPasswordRedis struct {
	UserId string `json:"user_id"`
	Email  string `json:"email"`
	Code   string `json:"code"`
}

type CreateBoxReq struct {
	PostId string `json:"post_id"`
}

type VerifyUserResponse struct {
	AccessToken string `json:"access_token"`
}
