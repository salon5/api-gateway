package models

type BusinessAddresses struct {
	Country string  `json:"country"`
	Region  string  `json:"region"`
	City    string  `json:"city"`
	Lat     float64 `json:"lat"`
	Long    float64 `json:"long"`
}

type BusinessAddressesUpdateReq struct {
	Id      string  `json:"id"`
	Country string  `json:"country"`
	Region  string  `json:"region"`
	City    string  `json:"city"`
	Lat     float64 `json:"lat"`
	Long    float64 `json:"long"`
}

type DeleteBusinessAddressReq struct {
	AddressId string `json:"address_id"`
}

type UpdateBusinessProfileReq struct {
	CompanyName string   `json:"company_name"`
	Bio         string   `json:"bio"`
	PhoneNumber []string `json:"phone_numbers"`
	Logo        string   `json:"logo"`
}

type AddSocialMediasToBusinessReq struct {
	BusinessId      string `json:"business_id"`
	SocialMediaName string `json:"social_media_name"`
	Link            string `json:"link"`
}

type SocialMediasCreteReq struct {
	SocialMediaName string `json:"social_media_name"`
	Link            string `json:"link"`
}

type BusinessCreateReq struct {
	CompanyName  string                 `json:"company_name"`
	Logo         string                 `json:"logo"`
	Bio          string                 `json:"bio"`
	PhoneNumbers []string               `json:"phone_numbers"`
	SocialMedias []SocialMediasCreteReq `json:"social_medias"`
	Addresses    []BusinessAddresses    `json:"addresses"`
}

type BusinessAddressCreateReq struct {
	BusinessId string  `json:"business_id"`
	Country    string  `json:"country"`
	Region     string  `json:"region"`
	City       string  `json:"city"`
	Lat        float64 `json:"lat"`
	Long       float64 `json:"long"`
}
