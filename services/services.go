package services

import (
	"fmt"

	"gitlab.com/salon5/api-gateway/config"
	"gitlab.com/salon5/api-gateway/genproto/autoinfo"
	"gitlab.com/salon5/api-gateway/genproto/customer"
	"gitlab.com/salon5/api-gateway/genproto/salon"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	CustomerService() customer.CustomerServiceClient
	SalonService()	salon.SalonServiceClient
	AutoInfoService() autoinfo.AutoInfoServiceClient
}

type serviceManager struct {
	customerService customer.CustomerServiceClient
	salonService salon.SalonServiceClient
	autoInfoService autoinfo.AutoInfoServiceClient
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	customerConn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.CustomerServiceHost, conf.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &serviceManager{}, err
	}

	salonConn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.SalonServiceHost, conf.SalonServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &serviceManager{}, err
	}

	autoInfoConn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.AutoInfoServiceHost, conf.AutoInfoServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &serviceManager{}, err
	}

	serviceManager := &serviceManager{
		customerService: customer.NewCustomerServiceClient(customerConn),
		salonService: salon.NewSalonServiceClient(salonConn),
		autoInfoService: autoinfo.NewAutoInfoServiceClient(autoInfoConn),
	}

	return serviceManager, nil
}

func (s *serviceManager) CustomerService() customer.CustomerServiceClient {
	return s.customerService
}

func (s *serviceManager) SalonService() salon.SalonServiceClient {
	return s.salonService
}

func (s *serviceManager) AutoInfoService() autoinfo.AutoInfoServiceClient {
	return s.autoInfoService
}